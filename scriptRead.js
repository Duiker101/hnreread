var localStorageKeyRead = "hnrr";
var storageRead = getStorage(localStorageKeyRead);

highligth();

function highligth(){
  if(!storageRead["page"]) storageRead["page"] = {};
  if(!storageRead["view"]) storageRead["view"] = {};
  if(!storageRead["page"]["score"]) storageRead["page"]["score"] = {};
  if(!storageRead["page"]["comm"]) storageRead["page"]["comm"] = {};
  if(!storageRead["view"]["score"]) storageRead["view"]["score"] = {};
  if(!storageRead["view"]["comm"]) storageRead["view"]["comm"] = {};
 
  if(document.URL.indexOf("item") >= 0){
    saveDiscussion();
  }else{
    loadPage();
    savePage();
  }
  
  clearStorage(storageRead["page"]["score"]);
  clearStorage(storageRead["page"]["comm"]);
  clearStorage(storageRead["view"]["score"]);
  clearStorage(storageRead["view"]["comm"]);
  saveStorage(localStorageKeyRead,storageRead);
}

function savePage(){
  $("span[id^=score_]").each(function(){
    var id = this.id.replace("score_","");
    storageRead["page"]["score"][id]=parseScore(this,"point"); 
  });
  $(".subtext a[href^=item]").each(function(){
    var id = $(this).attr("href").replace("item?id=","");
    storageRead["page"]["comm"][id]=parseScore(this,"comment"); 
  });
}

function saveDiscussion(){
  $(".subtext span[id^=score_]").each(function(){
    var id = this.id.replace("score_","");
    storageRead["view"]["score"][id]=parseScore(this,"point"); 
  });
  $(".subtext a[href^=item]").each(function(){
    var id = $(this).attr("href").replace("item?id=","");
    storageRead["view"]["comm"][id]=parseScore(this,"comment"); 
  });
}

function parseScore(ele,index){
    var commPos = $(ele).text().indexOf(index);
    var number = 0;
    if(commPos >= 0)
      number = parseInt($(ele).text().substring(0,commPos));
	return number;
}

function loadPage(){
  $("span[id^=score_]").each(function(){
    genDifference(this,this.id.replace("score_",""),parseScore(this,"point"),"score");
  });
  $(".subtext a[href^=item]").each(function(){
    genDifference(this,$(this).attr("href").replace("item?id=",""),parseScore(this,"comment"),"comm");
  });
}

function genDifference(par,id,currScore,type){
    var discScore = storageRead["page"][type][id];
    var viewScore = storageRead["view"][type][id];

    if(viewScore && currScore-viewScore > 0)
      $(par).after("<span class='plusView'>+"+(currScore-viewScore)+"</span>"); 
    if(discScore && currScore-discScore > 0)
      $(par).after("<span class='plusPage'>+"+(currScore-discScore)+"</span>"); 
}

function clearStorage(obj){
	var j = 0;
	var ids = [];
	for(a in obj)
		ids.push(parseInt(a));
	if(ids.length < 500)
		return;
	ids.sort().reverse();
	ids.splice(0,500);
	
	for(i in ids){
		delete obj[ids[i]];
	}
}

